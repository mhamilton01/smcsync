package com.viken.smcsync;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.viken.motorlib.ChopperControl;

public class MainActivity extends AppCompatActivity {

    private android.os.Handler updateHandler;
    private Runnable updateRunnable;
    private int DELAYTIME = 500;
    private ChopperControl cc;
    private TextView chopperStatus;
    private Thread cycleThread;
    private boolean actuallyEnabled = false;
    private Button chopperCycle;
    private String host = "192.168.1.45";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        cc = new ChopperControl(host);
        TextView chopperInfo = findViewById(R.id.chopperInfo);
        chopperStatus = findViewById(R.id.chopperStatus);
        final com.viken.smcsync.WrapperSpinner chopperSpeed = findViewById(R.id.chopperSpeed);
        final com.viken.smcsync.WrapperSpinner chopperCycleCount = findViewById(R.id.chopperCycleCount);
        final Button chopperEnable = findViewById(R.id.chopperEnable);
        chopperEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chopperEnable.getText().equals("Enable")) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (cc.enable(Integer.valueOf(chopperSpeed.getSelectedItem().toString()))) {
                                actuallyEnabled = true;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        chopperEnable.setText("Disable");
                                        chopperStatus.setText(chopperSpeed.getSelectedItem().toString());
                                    }
                                });
                            }
                            //if (!actuallyEnabled)
                        }
                    }).start();
                } else {
                    chopperEnable.setText("Enable");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            cc.disable();
                            actuallyEnabled = false;
                        }
                    }).start();
                }
            }
        });

        chopperCycle = findViewById(R.id.chopperCycle);
        chopperCycle.setText("Cycle Enable");
        chopperCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chopperCycle.getText().equals("Cycle Enable")) {
                    chopperCycle.setText("Cycle Disable");
                    cycleThread = chopperCycle(Integer.valueOf(chopperCycleCount.getSelectedItem().toString()),
                            Integer.valueOf(chopperSpeed.getSelectedItem().toString()), chopperStatus);
                } else {
                    chopperCycle.setText("Cycle Enable");
                    cycleThread.interrupt();
                }
            }
        });

        Button chopperTest = findViewById(R.id.chopperTest);
        chopperTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        cc.Test();
                    }
                }).start();
            }
        });
    }
    private void updateThread(TextView tv) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int curr = cc.getCurrent();
                int rpm = cc.getRpm();
                runOnUiThread(() -> {
                    String text = "RPM (avg) " + rpm + "\n" + "Curr (avg) " + curr;
                    tv.setText(text);
                });
            }
        }).start();
    }

    private Thread chopperCycle(int cnt, int rpm, TextView tv) {
        final int delay = 100;
        Thread th = new Thread() {
            int thCnt = 1;
            public void run() {
                for (int i = 0; i < cnt; i++) {
                    uiText(tv, "enable to " + rpm);
                    cc.enable(rpm);
                    while(cc.getRpm() < rpm) {
                        try {Thread.sleep(delay);} catch (InterruptedException e) { return; }
                    }
                    uiText(tv, "disable, cnt " + thCnt++);
                    cc.disable();
                    while(cc.getRpm() > 30) {
                        try {Thread.sleep(delay);} catch (InterruptedException e) { return; }
                    }
                }
                chopperCycle.setText("Cycle Enable");
                uiText(tv, "Done with " + thCnt);
            }
        };
        th.start();
        return th;
    }

    private void uiText(TextView tv, String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv.setText(s);
            }
        });
    }

}
